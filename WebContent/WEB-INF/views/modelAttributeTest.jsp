<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>@ModelAttribute Test</title>
</head>
<body>
<div style="text-align:center;">ModelAttribute Test Results<br>
<c:if test="${testdata5 != null && testdata5B != null}">
	<h3>city: ${testdata5}</h3>
	<h3>zip code: ${testdata5B}</h3>
</c:if>
<c:if test="${testdata6 != null}">
Relocated to: city: ${testdata6.city }, ${testdata6.zipCode}
</c:if>
</div>
</body>
</html>